import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */
public class Lfraction implements Comparable<Lfraction> {

   /** Main method. Different tests. */
   public static void main (String[] param) {
      Lfraction fraction = new Lfraction(-3,5);
      System.out.println(fraction.getNumerator() + " " + fraction.getDenominator());
   }

   private long numerator;
   private long denominator;

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      if (b == 0){
         throw new RuntimeException("Denominator cannot be 0!");
      }

      if (b<0 && a>0){
         b=b*(-1);
         a=a*(-1);
      }
      if (a<0 && b<0){
         b=b*(-1);
         a=a*(-1);
      }

      long gcd = gcd(a,b);
      numerator = a/gcd;
      denominator = b/gcd;

      if (denominator<0 && numerator>0){
         denominator=denominator*(-1);
         numerator=numerator*(-1);
      }
      if (numerator<0 && denominator<0){
         denominator=denominator*(-1);
         numerator=numerator*(-1);
      }

   }

   public static long gcd(long a, long b){
      if(b==0){
         return a;
      }

      return gcd(b, a%b);

   }

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return numerator;
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return denominator;
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(numerator+"/"+denominator);
      return sb.toString();
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
      if (m == this){
         return true;
      }
      if (!(m instanceof Lfraction)) return false;

      if (this.compareTo((Lfraction)m)==0){
         return true;
      }
      return false;
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
      long prime = 31;
      long result = 1;
      result = prime * result + denominator;
      result = prime * result + numerator;
      return (int)result;
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {

      long commonDenominator = m.denominator * denominator;
      long commonNumerator = m.numerator*denominator + numerator*m.denominator;
      return new Lfraction(commonNumerator, commonDenominator);
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
      return new Lfraction(m.numerator*numerator, m.denominator*denominator);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
      if (numerator == 0){
         throw new ArithmeticException("Cannot perform division by 0!");
      }
      return new Lfraction(denominator,numerator);
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
      return new Lfraction(-1*numerator, denominator);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
      Lfraction result = this.plus(m.opposite());
      return result;
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
      if (m.numerator == 0){
         throw new ArithmeticException("Cannot perform division by zero!");
      }
      Lfraction result = this.times(m.inverse());
      return result;
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
      Lfraction result = minus(m);
      if (result.numerator >0){
         return 1;
      }else if (result.numerator == 0){
         return 0;
      }else {
         return -1;
      }

   }

   /** Clone of the fraction.
    * @return new fraction equal to thisreturn null;
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Lfraction(numerator,denominator);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      if (numerator < 0){
         return (long)Math.ceil(numerator/denominator);
      }else {
         return (long)Math.floor(numerator/denominator);
      }
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      return new Lfraction(numerator%denominator, denominator); // TODO!!!
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      return (double)numerator / (double)denominator;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      int numerator = (int)Math.round(f * d);
      return new Lfraction(numerator,d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
   public static Lfraction valueOf (String s) {
      try{
         StringTokenizer tokens = new StringTokenizer(s,"/");
         return new Lfraction(Long.parseLong(tokens.nextToken()), Long.parseLong(tokens.nextToken()));
      }catch(IllegalArgumentException e){
         System.out.println(e.toString()+"String "+s+" is invalid!");
      }
      throw new IllegalArgumentException("String "+s+"contained illegl characters!");
   }
}

